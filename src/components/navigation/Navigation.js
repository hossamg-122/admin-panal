import React from 'react';
import {Route,useHistory} from 'react-router-dom'
import { makeStyles,Grid, Container, Button } from '@material-ui/core';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import SportsEsportsOutlinedIcon from '@material-ui/icons/SportsEsportsOutlined';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import TuneOutlinedIcon from '@material-ui/icons/TuneOutlined';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import sword from '../media/sword.png'
import Games from '../Games/Games'
import CreateGame from '../Games/CreateGame/CreateGame';
import Users from '../users/Users'
import Header from './Header'
import { connect } from 'react-redux';
import {signOut} from '../../store/actions'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    background:'linear-gradient(90deg, #4A3B85 20.69%, #643FDB 100%)',
    height:'240px',
    zIndex:'1 !important'
  },
  drawer: {
   
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: `${theme.spacing(3)}px  0`,
  },
  image:{
      width:'60px',
      height:'60px'
  },
  title:{
      marginTop:'15px',
      marginBottom:'50px'
  },
  button:{
      position:'relative',
      top:"500px"
  }
}));

const Navigation = ({signOut}) => {
  const classes = useStyles();
  const router = useHistory();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Header/>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.toolbar} >
            <Grid className={classes.title} container justifyContent='center' alignItems='center' align='center' >
                <Grid item xs={4}>
                    <img className={classes.image} src={sword} alt='logo' />
                </Grid>
                <Grid item xs={4}>
                <Typography variant='h5' color='secondary' gutterBottom >
                <b>Logo</b>
           </Typography>
                </Grid>
            </Grid>
            
          
        </div>
        <List>
          {[
              {text:'Games',
                icon:<SportsEsportsOutlinedIcon fontSize='large' color='secondary'  />},
               {text:'Users',
                icon:<PeopleAltOutlinedIcon fontSize='large'color='secondary'/>},
                {text:'Control',
                    icon:<TuneOutlinedIcon fontSize='large' color='secondary'/>},
                 {text:'Settings',
                    icon:<SettingsOutlinedIcon fontSize='large' color='secondary'/>}
                ].map(({text, icon}) => (
            <ListItem button key={text} onClick={()=>{router.push(`/${text.toLowerCase()}`)}} >
              <ListItemIcon >{icon}</ListItemIcon>
              <Typography  color='secondary'>{text}</Typography>
            </ListItem>
          ))}
           
        </List>
        <Button
                className={classes.button}
              startIcon={<ExitToAppIcon />}
              onClick={() => {
                signOut();
              }}
             
              color="secondary"
              size="mediem"
            >
              Logout
            </Button>
      </Drawer>
    
      <Container className={classes.content}>
        <div className={classes.toolbar} />
        
        <Route path='/games' exact component={Games} />
        <Route path='/users' exact component={Users} />
        <Route path='/games/create' exact component={CreateGame} />
      </Container>
      </div>
  );
}
export default connect(null,{signOut}) (Navigation)