import React from "react";
import { Grid, Typography, Paper, Container, TextField } from "@material-ui/core";
import ContentHeader from "../../ContentHeader/ContentHeader";
import makeStyles from "./style";
import { Formik, Form } from "formik";
import TextHandler from "./TextHandler";
const CreateGame = () => {
  const useStyles = makeStyles;
  const classes = useStyles();
  const INITIAL_VALUES = {
    gameTitle: "",
    description: "",
    price:'',
    date:'',
    timeFrom:"",
    timeTo:''
  };
  return (
    <Paper className={classes.root}>
      <ContentHeader header="Create New Game" />
      <Container>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="h5">General info</Typography>
          </Grid>
          <Grid item xs={12}>
            <Formik
            initialValues={{...INITIAL_VALUES}}
            >
              {(formprops) => (
                <Form>
                  <Grid container>
                    <Grid container item xs={3}>
                      left
                    </Grid>
                    <Grid container justifyContent="center" item xs={9}>
                      <TextHandler
                       
                        name="gameTitle"
                        type="text"
                        placeholder="Title"
                        label="Game Title"
                     />
                      <TextHandler
                        
                        name="description"
                        type='text'
                        multiline
                        rows={5}
                        placeholder="Write The Description"
                        label="Description"
                      />
                      <TextHandler
                     
                        name="price"
                        type='text'
                        placeholder="Price"
                        label="Price"
                     />
                      <TextHandler
                        name="date"
                        type='date'
                        label="Date"
                      />
                       <TextHandler
                        name="time"
                        type='time'
                        label="Time"
                      />
                     
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </Grid>
      </Container>
    </Paper>
  );
};
export default CreateGame;
