import React from "react";
import { ErrorMessage, useField } from "formik";
import { TextField, Typography,Grid,InputAdornment } from "@material-ui/core";

const TextHandler = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  {console.log({...props})}
  return (
    
    <Grid container item={12} alignItems='center' >
    {label==='Time'?
    <React.Fragment>
    <Grid item xs ={2}>
    <Typography variant="subtitle1" color="secondary" gutterBottom={true}>
      <b>{label}</b>{" "}
    </Typography>
    </Grid>
    <Grid xs={10} item container justifyContent='space-between' >

   
    <Grid item xs={5}>
    <TextField
      error={meta.touched && meta.error ? true : false}
      color="secondary"
      variant="outlined"
      {...field}
      {...props}
      fullWidth={true}
      size="medium"
      autoComplete="off"
      helperText={<ErrorMessage name={field.name} />}
      InputProps={{
        startAdornment: <InputAdornment position="start">From</InputAdornment>,
      }}
    />
    </Grid>
    <Grid item xs={5}  >
    <TextField
      error={meta.touched && meta.error ? true : false}
      color="secondary"
      variant="outlined"
      {...field}
      {...props}
      fullWidth={true}
      size="medium"
      autoComplete="off"
      helperText={<ErrorMessage name={field.name} />}
      InputProps={{
        startAdornment: <InputAdornment position="start">To</InputAdornment>,
      }}
    />
    </Grid>
    </Grid>
    </React.Fragment>
    :
    <React.Fragment>
     
    <Grid item xs ={2}>
    <Typography variant="subtitle1" color="secondary" gutterBottom={true}>
      <b>{label}</b>{" "}
    </Typography>
    </Grid>
    <Grid item xs={10}>
    
    <TextField
      error={meta.touched && meta.error ? true : false}
      color="secondary"
      variant="outlined"
      {...field}
      {...props}
      fullWidth={true}
      size="medium"
      autoComplete="off"
      helperText={<ErrorMessage name={field.name} />}
    />
    </Grid>
    </React.Fragment>
}
  </Grid>
  );
};
export default TextHandler;
