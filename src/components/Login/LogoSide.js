import React from "react";
import { Grid, Typography } from "@material-ui/core";
import makeStyles from "./style";
import sword from "../media/sword.png";

const LogoSide = () => {
  const useStyles = makeStyles;
  const classes = useStyles();
  return (
    <Grid item xs={6} container className={classes.logo} alignItems="center">
      <Grid item xs={2} />
      <Grid item xs={8} className={classes.logoBody} align="center" container>
        <Grid item xs={12} container alignItems="center">
          <Grid item xs={6}>
            <img src={sword} className={classes.logoImage} alt='logo' />
          </Grid>
          <Grid item xs={6} container align="center">
            <Typography variant="h2" color="primary">
              <b>Logo</b>
            </Typography>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h6" align="center" color="primary">
            Create, manage and track Cluerush
            <br />
            games from one place
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="body2" color="primary">
            This Admin Panel is only accessible for thee who can <br />
            create fun from murders
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={2} />
    </Grid>
  );
};
export default LogoSide;
