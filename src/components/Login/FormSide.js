import React from "react";
import { Link, Grid, Typography, Button } from "@material-ui/core";
import { Formik, Form } from "formik";
import makeStyles from "./style";
import * as Yup from "yup";
import TextHandler from "./TextHandler";
import { connect } from "react-redux";
import { signIn } from "../../store/actions";
const useStyles = makeStyles;

const INITIAL_VALUES = {
  userName: "",
  password: "",
};
const validate = Yup.object({
  userName: Yup.string().required("required").email("Email is invalid"),
  password: Yup.string().required("required"),
});
const FormSide = ({ signIn, error }) => {
  const classes = useStyles();
  return (
    <Grid item xs={6} container className={classes.form} alignItems="center">
      <Grid item xs={3} />
      <Grid item xs={6} className={classes.formBody} container>
        <Grid
          className={classes.input}
          item
          xs={12}
          container
          direction="column"
          justifyContent="center"
        >
          <Typography variant="h5" color="secondary" gutterBottom={true}>
            <b>Welcom back!</b>
          </Typography>
          <Typography variant="body2" color="secondary" gutterBottom={true}>
            Please login to access your account
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Formik
            initialValues={{ ...INITIAL_VALUES }}
            validationSchema={validate}
            onSubmit={(values) => {
              console.log(values);
              signIn(values);
            }}
          >
            {(formikProps) => (
              <Form>
                {error ? (
                  <Typography variant="body2" color="error">
                    Invalid User Name or Password
                  </Typography>
                ) : null}
                <TextHandler
                  className={classes.input}
                  placeholder="Type Your Email Or Password"
                  name="userName"
                  type="email"
                  label="E-mail Or Username"
                ></TextHandler>
                <TextHandler
                  className={classes.input}
                  name="password"
                  type="password"
                  placeholder="Type your password"
                  label="Password"
                ></TextHandler>
                <Link href="#" color="secondary">
                  <b>Forgot Password?</b>
                </Link>
                <Button
                  className={classes.button}
                  color="secondary"
                  fullWidth={true}
                  variant="outlined"
                  type="submit"
                >
                  Login
                </Button>
              </Form>
            )}
          </Formik>
        </Grid>
        <Grid item xs={12}></Grid>
      </Grid>
      <Grid item xs={3} />
    </Grid>
  );
};
const mapStateToProps = (state) => {
  return {
    error: state.auth.authError,
  };
};
export default connect(mapStateToProps, { signIn })(FormSide);
