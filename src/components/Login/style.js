import {makeStyles} from "@material-ui/core";
import cover from '../media/cover.png'
export default makeStyles({
    logo: {
      backgroundImage:`url(${cover}) `,
      backgroundRepeat:'no-repeat',
      backgroundSize:'cover',
      backgroundPosition:'center center',
      minHeight: "800px",
     
      width:'720px',
  
      margin: "0 auto",
    },
    logoBody: {
      height: "50%",
      width: "100%",
    },
    logoImage: {
      width: "100px",
      height: "100px",
      borderRadius: "20px 20px",
    },
    form: {
        backgroundColor: "#f4f7fe",
        minHeight: "800px",
        width:'720px',
        height:'100vh',
        margin: "0 auto",
      },
      formBody: {
        height: "50%",
        width: "100%",
      },
      formImage: {
        width: "100px",
        height: "100px",
        borderRadius: "20px 20px",
      },
      input:{
        marginBottom:'30px',
      },
      button:{
        marginTop:'50px',
        backgroundColor:'#4a3b85',
      }
  });
  
   