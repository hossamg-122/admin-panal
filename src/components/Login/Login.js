import React from 'react'
import { Grid } from '@material-ui/core'
import LogoSide from './LogoSide'
import FormSide from './FormSide'


const Login = () =>{

    return (
        
        <Grid container >
             <LogoSide/>
             <FormSide/>
        </Grid>
            
     
      
    )
}
export default Login