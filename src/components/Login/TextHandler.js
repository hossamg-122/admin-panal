import React from "react";
import { ErrorMessage, useField } from "formik";
import { TextField, Typography } from "@material-ui/core";

const TextHandler = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  
  return (
    <div>
      <Typography variant="subtitle1" color="secondary" gutterBottom={true}>
        <b>{label}</b>{" "}
      </Typography>
      <TextField
        error={meta.touched && meta.error ? true : false}
        color="secondary"
        variant="outlined"
        {...field}
        {...props}
        fullWidth={true}
        size="medium"
        autoComplete="off"
        helperText={<ErrorMessage name={field.name} />}
      />
    </div>
  );
};
export default TextHandler;
