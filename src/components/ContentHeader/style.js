import { makeStyles } from "@material-ui/core"
export default makeStyles({
    root: {
        width: '100%',
        position:'relative',
        top:'-5px',
        zIndex:'5 !important'
        
      },
      container: {
        maxHeight: 440,
      },
      header:{
        background:'linear-gradient(90deg, #4A3B85 20.69%, #643FDB 100%)',
        color:"#ffffff",
        height:'100px'
      },
      button:{
        backgroundColor:"#ffffff",
        borderRadius:'15px',
        '&:hover' :{
            backgroundColor: "#FFF"
          }
      }
})