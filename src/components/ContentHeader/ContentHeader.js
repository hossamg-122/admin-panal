import React from 'react'
import {Grid,Typography,Button,Paper} from '@material-ui/core'
import {useHistory} from 'react-router-dom'
import makeStyles from "./style";
 const ContentHeader=({header,button,path})=>{
    const useStyles = makeStyles;
    const classes = useStyles();
    const router = useHistory()
    return(
        
                <Grid container className={classes.header} alignItems='center' >
            <Grid item xs={6}>
            <Typography variant='h4' > 
            {header}
            </Typography> 
            </Grid>
            {button&&path?
            <Grid container item xs={6} justifyContent='flex-end' >
            <Button className={classes.button} variant='outlined' onClick={()=>{router.push(path)}} >{button}</Button>
            </Grid>:null
        }
            
        </Grid>
       
        
    )
 }
 export default ContentHeader