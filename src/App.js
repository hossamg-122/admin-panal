import React from "react";
import { connect } from "react-redux";
import Login from "./components/Login/Login";
import Navigation from "./components/navigation/Navigation";
import Games from "./components/Games/Games";
import { BrowserRouter, Route } from "react-router-dom";
import { Grid, Container, ThemeProvider, createTheme } from "@material-ui/core";
const theme = createTheme({
  palette: {
    primary: {
      main: "#ffffff",
    },
    secondary: {
      main: "#1b2559",
    },
  },
});
const App = ({ auth }) => {
  console.log(auth);

  return (
    <div>
      <ThemeProvider theme={theme}>
        {auth.uid ? (
          <Container>
            <BrowserRouter>
              <Navigation />
            </BrowserRouter>
          </Container>
        ) : (
          <Login />
        )}
      </ThemeProvider>
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
  };
};
export default connect(mapStateToProps)(App);
