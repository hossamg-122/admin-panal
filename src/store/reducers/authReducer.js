import { LOGIN_SUCCESS, LOGIN_ERROR,SIGNOUT_SUCESS} from "../actions/type";
const initState={
    authError:null
}
const authReducer = (state=initState,action) =>{
    switch (action.type) {
        case LOGIN_ERROR:
            console.log('login-error')
           return {...state,authError:action.payload}
        case LOGIN_SUCCESS:
            console.log('login-success')
            return {...state,authError:null}
        case SIGNOUT_SUCESS :
            console.log('logout-success')
            return state
        default:
            return state
    }
}
export default authReducer