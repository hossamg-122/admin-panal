import { LOGIN_SUCCESS,LOGIN_ERROR,SIGNOUT_SUCESS } from "./type";

export const signIn = ({userName,password}) =>{
    ;
    return (dispatch,getState,{getFirebase})=>{
        getFirebase().auth().signInWithEmailAndPassword(
           userName,
            password
        ).then(()=>{
            dispatch({type:LOGIN_SUCCESS})
            
        }).catch((err)=>{
            dispatch({type:LOGIN_ERROR,payload:err})
        })
    }

}
export const signOut = () => {
    return(dispatch,getState,{getFirebase})=>{
        getFirebase().auth().signOut().then(()=>{
            dispatch({
                type:SIGNOUT_SUCESS
            })
        })
    }
}