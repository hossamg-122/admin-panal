import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
var firebaseConfig = {
    apiKey: "AIzaSyB6pr8nRqluY-JNCVAS5ZezgxSYK4U1k4M",
    authDomain: "cr-mobile-13fb4.firebaseapp.com",
    projectId: "cr-mobile-13fb4",
    storageBucket: "cr-mobile-13fb4.appspot.com",
    messagingSenderId: "92019115699",
    appId: "1:92019115699:web:c2b2d89530890e7b4992c6",
    measurementId: "G-FMFYPR2GG2"
  };

   firebase.initializeApp(firebaseConfig);
   firebase.firestore().settings({timestampsInSnapshots:true })
 export default firebase;