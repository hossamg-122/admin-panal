import React from 'react';
import ReactDOM from 'react-dom';
import reducers from './store/reducers'
import App from './App';
import fbConfig from './firebase/fire'
import { createStore,applyMiddleware,compose } from 'redux';
import thunk from 'redux-thunk'
import { Provider } from 'react-redux';
import { getFirestore, reduxFirestore } from 'redux-firestore';
import { getFirebase, reactReduxFirebase} from 'react-redux-firebase';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers,
                          composeEnhancers(
                            applyMiddleware(thunk.withExtraArgument({getFirebase,getFirestore})),
                            reduxFirestore(fbConfig),
                            reactReduxFirebase(fbConfig,{attachAuthIsReady:true}),

                          ))
store.firebaseAuthIsReady.then(()=>{
  ReactDOM.render(
    <Provider store={store} >
        <App  />
    </Provider>
    ,
    document.getElementById('root')
  );
  
})

